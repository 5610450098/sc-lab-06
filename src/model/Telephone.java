package model;

public abstract class Telephone {
	private String number;
	private String namePhone;
	private String makeIn = "USA";
	public Telephone(){
		namePhone = "ID Phone";
	}
	public Telephone(String number,String name){
		this.number = number;
		this.namePhone = name;
	}
	
	public abstract String showBand();
	public abstract String showBand(String version);
	public String showNumber(){
		return number;
	}
	public String showName(){
		return namePhone;
	}
	public String produce(String product){
		String makeIn = "China";
		if(product == "iPhone"){
			return makeIn;
		}
		else if (product == "Sumsung") {
			return this.makeIn;
		}
		else{
			return "";
		}
	
	}

}
