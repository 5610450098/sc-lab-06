package model;


public class Sumsung extends Telephone {
	public Sumsung(){
		super();
	}
	public Sumsung(String namePhone,String number){
		super(namePhone,number);
	}
	public String showBand(){
		return "Sumsung";
	}
	public String showBand(String version){
		return "Sumsung"+version;
	}
}