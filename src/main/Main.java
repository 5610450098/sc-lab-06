package main;

import model.Iphone;
import model.Sumsung;
import model.Telephone;
import model.TelephoneBand;

public class Main {
	public static void main(String[] args){
		Sumsung s1 = new Sumsung();
		Iphone i1 = new Iphone();
		
		Sumsung s2 = new Sumsung("0922349998","BLACK");
		Iphone i2 = new Iphone("0951116789", "BOSS");
		TelephoneBand band = new TelephoneBand();
		
		Sumsung s3 = new Sumsung("0922349998","Black");
		Iphone i3 = new Iphone("0951116789", "Boss");
		
		System.out.println("--------Test 1--------");
		System.out.println(s1.showName());
		System.out.println(i1.showName());
		
		System.out.println("--------Test 2--------");
		System.out.println(band.showBand(s2));
		System.out.println(s2.showBand("NOTE3"));
		System.out.println(s2.showNumber()+"\t"+s2.showName()+"\n");
		System.out.println(band.showBand(i2));
		System.out.println(i2.showBand("6Plus"));
		System.out.println(i2.showNumber()+"\t"+i2.showName()+"\n");
		System.out.println(band.showBand((Telephone)s2));
		System.out.println(band.showBand((Telephone)i2));
		
		System.out.println("--------Test 3--------");
		System.out.println(i1.showBand()+" make in :"+i1.produce(i1.showBand()));
		System.out.println(s1.showBand()+" make in :"+s1.produce(s1.showBand()));
	}
}
